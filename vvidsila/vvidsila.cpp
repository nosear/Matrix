// vvidsila.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "iostream"
#include "windows.h"
#include <time.h>
#include <string.h>

using namespace std;

//Равны ли массивы. Для тестов
bool areEqualArrays(int** arrayA, int m, int n, int** arrayB, int p, int k)
{
	bool equal = true;

	if ((m == p) && (n == k))
	{
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				if (arrayA[i][j] != arrayB[i][j])
				{
					equal = false;
					break;
				}
			}

			if (!equal)
			{
				break;
			}
		}
	}

	else
	{
		equal = false;
	}

	return equal;
}
//Функция вывода меню для разных пунктов
void menu(int menuType)
{
	system("cls");

	switch (menuType)
	{
		//Главное меню
	case 1:
	{
		cout << "1. Создание массивов." << endl;
		cout << "2. Заполнение массивов." << endl;
		cout << "3. Вывод массивов." << endl;
		cout << "4. Поиск элемента массива." << endl;
		cout << "5. Умножение элементов на Х." << endl;
		cout << "6. Умножение матриц друг на друга." << endl;
		cout << "7. Сложение матриц." << endl;
		cout << "8. Транспонирование матрицы." << endl;
		cout << "9. Вычисление среднего арифметического" << endl;
		cout << "0. Выход." << endl;
		cout << "" << endl;
		break;
	}

	//Меню выбора массива
	case 2:
	{
		cout << "1. Массив А." << endl;
		cout << "2. Массив Б." << endl;
		break;
	}

	//Меню заполнения массива (ручной или рандомно)
	case 3:
	{
		cout << "1. Заполнить массив вручную." << endl;
		cout << "2. Заполнить массив случайными числами." << endl;
		break;
	}
	}
}
//Функция вывода на экран массива
void showArray(int** array, int m, int n, char letter)
{
	cout << "Ваш массив " << letter << ":" << endl;

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			cout << array[i][j] << "	";
		}

		cout << endl << endl;
	}
}
//Функция удаления массива
int** deleteArray(int** array, int m)
{
	if (array != NULL)
	{
		for (int i = 0; i < m; i++)
		{
			delete[] array[i];
		}
		delete[] array;
	}

	return array;
}
//Функция для проверки пользовательского ввода
int numericInput()
{
	int choice = -1;
	bool passed = false;

	while (!passed)
	{
		cin >> choice;

		if (!cin)
		{
			cout << "Некорректный ввод \n";

			cin.clear();
			cin.ignore(65535, '\n');
		}

		else
		{
			passed = true;

			cin.clear();
			cin.ignore(65535, '\n');
		}
	}

	return choice;
}
//Функция транспонирования массива (Vladislav)
int** transpose(int** array, int m, int n)
{
	int** arrayC = NULL;

	arrayC = new int *[n];

	for (int i = 0; i < n; i++)
	{
		arrayC[i] = new int[m];
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			arrayC[i][j] = array[j][i];
		}
	}

	return arrayC;
}
//Функция вычисления среднего арифметического (Vladislav)
float arithMean(int** array, int m, int n)
{
	float srArith = 0;

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			srArith += array[i][j];
		}
	}

	srArith = srArith / (m * n);

	return srArith;
}








//Функция заполнения массива вручную
int** manualFill(int** array, int m, int n)
{
	system("cls");
	cout << "Введите элементы массива (целые числа):" << endl;

	for (int i = 0; i < m; i++)
	{
		cout << "Введите элементы строки № " << i + 1 << ":" << endl;

		for (int j = 0; j < n; j++)
		{
			array[i][j] = numericInput();
		}
	}

	return array;
}

//Функция заполнения псевдослучайными числами
int** randomFill(int** array, int m, int n)
{
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			array[i][j] = rand();
		}
	}

	return array;
}

//Функция создания массива
int** createArray(int** array, int& m, int& n)
{
	system("cls");
	deleteArray(array, m);

	m = 0;
	n = 0;

	while (m <= 0)
	{
		cout << "Введите количество строк в массиве (целое положительное число): " << endl;
		m = numericInput();
	}

	array = new int *[m];

	while (n <= 0)
	{
		cout << "Введите количество столбцов в массиве (целое положительное число): \n";
		n = numericInput();
	}

	for (int i = 0; i < m; i++)
	{
		array[i] = new int[n];
	}

	return array;
}
//Функция умножения массивов друг на друга (Vladislav)
int** multiplyArrays(int** arrayA, int m, int n, int** arrayB, int p, int k)
{
	int** arrayC = NULL;
	int temp;

	arrayC = new int *[m];

	for (int i = 0; i < m; i++)
	{
		arrayC[i] = new int[k];
	}

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < k; j++)
		{
			temp = 0;

			for (int z = 0; z < n; z++)
			{
				temp += arrayA[i][z] * arrayB[z][j];
			}

			arrayC[i][j] = temp;
		}

	}

	return arrayC;
}







int main()

{
	setlocale(LC_ALL, "Russian");
	setlocale(LC_ALL, "rus");

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	int **arrayA = NULL;
	int **arrayB = NULL;
	int **arrayC = NULL;
	int choice = -1;
	int m = 0, n = 0;
	int p = 0, k = 0;
	int multiplier = 0;
	bool exit = false;
	srand(time(NULL));

	while (!exit)
	{
		menu(1);

		choice = numericInput();

		switch (choice)
		{
			//Создание массивов
		case 1:
		{
			menu(2);

			choice = numericInput();

			//Выбор массива для создания
			switch (choice)
			{
			case 1:
			{
				arrayA = createArray(arrayA, m, n);
				break;
			}

			case 2:
			{
				arrayB = createArray(arrayB, p, k);
				break;
			}

			default:
			{
				cout << "Такого выбора нет" << endl;
				system("pause");
			}


			break;
			}

			//Заполнение массива
		case 2:
		{
			menu(2);

			choice = numericInput();

			//Заполняемый массив
			switch (choice)
			{
			case 1:
			{
				if (arrayA != NULL)
				{
					menu(3);

					choice = numericInput();

					//Вариант заполнения
					switch (choice)
					{
					case 1:
					{
						manualFill(arrayA, m, n);
						break;
					}

					case 2:
					{
						randomFill(arrayA, m, n);
						break;
					}

					default:
					{
						cout << "Такого выбора нет" << endl;
						system("pause");
					}
					}
				}

				else
				{
					cout << "Массив A не был задан. Пожалуйста, выберите другой пункт меню." << endl;
					system("pause");
				}

				break;
			}




			break;
			}

			//Вывод массивов
		case 3:
		{
			system("cls");

			if (arrayA != NULL)
			{
				showArray(arrayA, m, n, 'A');
			}

			else
			{
				cout << "Массив А не был задан" << endl;
			}

			if (arrayB != NULL)
			{
				showArray(arrayB, p, k, 'Б');
			}

			else
			{
				cout << "Массив Б не был задан" << endl;
			}

			system("pause");

			break;
		}
		// Выход
		case 0:
		{
			exit = true;
			break;
		}

		default:
		{
			cout << "Такого пункта меню нет. Пожалуйста, попробуйте снова.";
			getchar();
		}



		deleteArray(arrayA, m);
		deleteArray(arrayB, p);

		return 0;
		}
		//Вывод матрицы в транспонированном виде (Vladislav)
		case 8:
		{
			cout << "Выберите массив, который будет транспонирован:" << endl;
			menu(2);

			choice = numericInput();

			system("cls");

			switch (choice)
			{
			case 1:
			{
				if (arrayA != NULL)
				{
					arrayC = transpose(arrayA, m, n);

					cout << "Массив А в транспонированном виде: " << endl << endl;

					showArray(arrayC, n, m, 'A');

					deleteArray(arrayC, n);
				}

				else
				{
					cout << "Массив А не был задан" << endl << endl;
				}

				break;
			}

			case 2:
			{
				if (arrayB != NULL)
				{
					arrayC = transpose(arrayB, p, k);

					cout << "Массив Б в транспонированном виде: " << endl;

					showArray(arrayC, k, p, 'Б');

					deleteArray(arrayC, k);
				}

				else
				{
					cout << "Массив Б не был задан" << endl;
				}

				break;
			}

			default:
			{
				cout << "Такого выбора нет" << endl;
			}
			}

			system("pause");
			break;
		}
		}
		//Умножение матриц друг на друга (Vladislav)
		case 6:
		{
			system("cls");

			if ((n == p) && (arrayA != NULL))
			{
				cout << "Результатом произведения будет данная матрица:" << endl << endl;

				arrayC = multiplyArrays(arrayA, m, n, arrayB, p, k);
				showArray(arrayC, m, k, 'C');
				deleteArray(arrayC, m);
			}

			else
			{
				cout << "Матрицы не согласованы или не существуют, произведение невозможно" << endl;
			}

			system("pause");
			break;
		}



		case 9:
		{
			cout << "Выберите массив, в котором будет посчитано среднее арифметическое:" << endl;
			menu(2);

			choice = numericInput();

			system("cls");

			switch (choice)
			{
			case 1:
			{
				if (arrayA != NULL)
				{
					cout << "Среднее арифметическое элементов равно " << arithMean(arrayA, m, n) << endl;
				}

				else
				{
					cout << "Массив А не был задан" << endl;
				}

				break;
			}

			case 2:
			{
				if (arrayB != NULL)
				{
					cout << "Среднее арифметическое элементов равно " << arithMean(arrayB, p, k) << endl;
				}

				else
				{
					cout << "Массив Б не был задан" << endl;
				}

				break;
			}

			default:
			{
				cout << "Такого выбора нет" << endl;
			}
			}

			system("pause");
			break;
		}

		}
	}
}
	


	



		
	

